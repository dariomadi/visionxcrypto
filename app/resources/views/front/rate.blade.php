 @extends('include.front')
@section('content')
 <!-- Stat main -->
      <main data-spy="scroll" data-target="#navbar-example2" data-offset="0">
        <!-- Start banner_about -->
        <section class="pt_banner_inner banner_bg_pricing">
          <div class="container">
            <div class="row justify-content-center text-center">
              <div class="col-md-10 col-lg-6">
                <div class="banner_title_inner margin-b-3">
                  <h1 data-aos="fade-up" data-aos-delay="0">
                   Our Crypto Rates
                  </h1>
                  <p data-aos="fade-up" data-aos-delay="100">
                    Make mouth watering gain by trading your crypto assets with us
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- End banner_about -->

        <!-- Start p_pricing_list -->
        <section class="p_pricing_list">
          <div class="container">
            <!-- start tab -->
            <div class="row">
              <div class="col-lg-6 ml-auto">
                <div class="row justify-content-center">
                  <div class="col-lg-10">
                    <div class="tab_pricing_list">
                      <ul class="nav nav-pills" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                          <a class="nav-link active" id="pills-month-tab" data-toggle="pill" href="#pills-month"
                            role="tab" aria-controls="pills-month" aria-selected="true">Buy</a>
                        </li>
                        <li class="nav-item" role="presentation">
                          <a class="nav-link" id="pills-annually-tab" data-toggle="pill" href="#pills-annually"
                            role="tab" aria-controls="pills-annually" aria-selected="false">Sell</a>
                        </li>
                      </ul>
                      <br><br>

                      <div class="item_offer_annual">
                        <svg xmlns="http://www.w3.org/2000/svg" width="83.249" height="53.151"
                          viewBox="0 0 83.249 53.151">
                          <g id="Group_6567" data-name="Group 6567" transform="translate(-3.601 23.722) rotate(-17)">
                            <path id="Path_10061" data-name="Path 10061"
                              d="M347.272,426.7a3.578,3.578,0,0,0,1.105-.5,5.029,5.029,0,0,0,1.072-.95,6.522,6.522,0,0,0,.741-1.02,8.995,8.995,0,0,0,.51-1.126,10.545,10.545,0,0,0,.647-2.529,21.922,21.922,0,0,0,.23-3.02,17.053,17.053,0,0,0-.5-3.947,13.294,13.294,0,0,0-.353-1.292,13.712,13.712,0,0,0-1.162-2.456,10.45,10.45,0,0,0-1.578-1.972,13.932,13.932,0,0,0-1.233-1.109l.048-.074c.17-.264.375-.513.559-.769l.431-.591c.41-.5.805-1.015,1.251-1.477.357-.367.712-.735,1.07-1.1a16.843,16.843,0,0,1,1.684-1.5A19.273,19.273,0,0,1,355.5,399.1a17.666,17.666,0,0,1,4.012-1.121,17.006,17.006,0,0,1,3.92.054,15.7,15.7,0,0,1,3.566.946,12.735,12.735,0,0,1,2.425,1.4c.3.24.594.486.873.746-.093.078-.186.156-.271.241l-.82.8c-.16.158-.3.334-.451.5-.238.272-.481.538-.706.82a18.533,18.533,0,0,0-2.619,4.318,19.375,19.375,0,0,0-.907,2.505,12.263,12.263,0,0,0-.352,1.755,9.942,9.942,0,0,0,.057,2.643,7.274,7.274,0,0,0,2.423,4.359,8.735,8.735,0,0,0,.916.706,4.411,4.411,0,0,0,.792.422,7.626,7.626,0,0,0,1.207.419,8.433,8.433,0,0,0,1.1.174,4.357,4.357,0,0,0,1.242-.1,5.7,5.7,0,0,0,1.24-.443,4.593,4.593,0,0,0,1.108-.74,6.817,6.817,0,0,0,1.445-1.814,8.5,8.5,0,0,0,.543-1.207,7.182,7.182,0,0,0,.357-1.327c.086-.461.132-.93.192-1.393a6.139,6.139,0,0,0,.06-.759,16.64,16.64,0,0,0-.08-1.9c-.068-.673-.166-1.343-.282-2.011a22.528,22.528,0,0,0-1.38-4.913,15.544,15.544,0,0,0-1.334-2.5l.152-.122a16.9,16.9,0,0,1,3.268-1.906,16.274,16.274,0,0,1,3.7-1,30.034,30.034,0,0,1,3.366-.16c.925-.005,1.847.082,2.767.164l1.211.18c.459.068.914.194,1.367.29s.883.249,1.32.372.845.3,1.262.443l1.029.443c.386.166.756.391,1.128.585.349.182.676.414,1.012.624.309.192.6.422.9.637a18.8,18.8,0,0,1,1.6,1.39c.565.578,1.1,1.172,1.612,1.8.6.783,1.167,1.584,1.727,2.4a.586.586,0,0,0,.789.207.58.58,0,0,0,.207-.79,27.237,27.237,0,0,0-4.512-5.441,19.151,19.151,0,0,0-2.9-2.12c-.274-.155-.547-.311-.818-.463-.292-.166-.614-.292-.916-.437-.551-.262-1.126-.476-1.692-.7-.264-.106-.547-.184-.816-.274-.238-.08-.471-.16-.712-.225-.626-.168-1.245-.329-1.882-.463s-1.268-.231-1.91-.329c-.582-.091-1.178-.155-1.765-.191-.613-.036-1.227-.08-1.84-.088-.345,0-.686-.013-1.028-.005a19.314,19.314,0,0,0-2.643.277,16,16,0,0,0-4.76,1.7,15.244,15.244,0,0,0-1.837,1.167,14.968,14.968,0,0,0-1.492-1.3,12.943,12.943,0,0,0-2.371-1.483,16.251,16.251,0,0,0-4.776-1.439,23.63,23.63,0,0,0-2.568-.25,14.74,14.74,0,0,0-2.842.225,18.4,18.4,0,0,0-4.926,1.579,19.353,19.353,0,0,0-4.325,2.832c-.684.58-1.3,1.224-1.928,1.858-.417.419-.785.888-1.168,1.336a25.007,25.007,0,0,0-1.6,2.115,12,12,0,0,0-2.548-.867,10.1,10.1,0,0,0-5.425.295,14.466,14.466,0,0,0-4.569,2.664,12.857,12.857,0,0,0-1.6,1.657,18.388,18.388,0,0,0-4.121,8.606c-.323,1.121-.641,2.244-.937,3.373a1.6,1.6,0,0,0,3.093.851c.272-.986.562-1.97.85-2.953.433-1.486.839-2.985,1.417-4.423a18.443,18.443,0,0,1,2.1-3.482,13.453,13.453,0,0,1,2.033-2.059A13.74,13.74,0,0,1,337,407.282a9,9,0,0,1,1.857-.522,10.229,10.229,0,0,1,2.069-.005,10.413,10.413,0,0,1,1.867.492c-.078.135-.156.27-.234.406-.127.22-.269.437-.38.663-.158.324-.316.647-.476.968a22.555,22.555,0,0,0-1.445,3.668,18.148,18.148,0,0,0-.52,2.459,20.038,20.038,0,0,0-.231,2.78,13.469,13.469,0,0,0,.246,2.557,8.159,8.159,0,0,0,.909,2.384,8.457,8.457,0,0,0,1.483,1.905,8.121,8.121,0,0,0,1.051.833,6.817,6.817,0,0,0,1.281.639,4.706,4.706,0,0,0,1.375.305c.113.011.226.015.339.015a4.844,4.844,0,0,0,1.079-.132Zm-.994-2.827a4.672,4.672,0,0,1-.594-.176,5.675,5.675,0,0,1-.761-.451,7.315,7.315,0,0,1-.93-.944,8.352,8.352,0,0,1-.742-1.266,7.788,7.788,0,0,1-.386-1.444,16.607,16.607,0,0,1,.062-3.757,19.618,19.618,0,0,1,1.221-4.371c.236-.529.5-1.048.747-1.571.137-.29.316-.569.474-.851.037-.068.074-.136.111-.2a12.972,12.972,0,0,1,1.708,1.709,10.823,10.823,0,0,1,1.106,1.96,13.506,13.506,0,0,1,.816,6.157c-.038.353-.079.7-.117,1.057a9.951,9.951,0,0,1-.516,2.044,8.579,8.579,0,0,1-.621,1.185,5.746,5.746,0,0,1-.626.624,3.576,3.576,0,0,1-.384.237c-.076.024-.15.044-.226.064-.114.005-.227,0-.342,0Zm24.673-5.759a6.011,6.011,0,0,1-.907-.267,5.25,5.25,0,0,1-.741-.416,9.828,9.828,0,0,1-1.072-1.087,6.75,6.75,0,0,1-.621-1.054,6.66,6.66,0,0,1-.32-1.175,9.856,9.856,0,0,1,.007-2,16.405,16.405,0,0,1,1.008-3.383,18.924,18.924,0,0,1,1.992-3.482c.231-.288.465-.577.7-.865s.471-.531.712-.79q.163-.175.331-.345a14.232,14.232,0,0,1,.957,1.782,23.571,23.571,0,0,1,1.3,5.072c.086.7.156,1.395.183,2.1a13.721,13.721,0,0,1-.168,2.278c.013-.106.028-.212.041-.319a8.835,8.835,0,0,1-.445,1.694,6.864,6.864,0,0,1-.639,1.1,6.152,6.152,0,0,1-.664.682,3.752,3.752,0,0,1-.372.228,5.986,5.986,0,0,1-.851.24,3.892,3.892,0,0,1-.431,0Zm-21.841-2.226c-.01-.063-.018-.127-.027-.19.009.063.018.127.027.19Zm-5.048-4.227.051-.117-.051.117Zm-7.291-4.284.1-.04-.1.04Z"
                              transform="translate(-321.141 -395.16)" fill="#0b2238" />
                            <path id="Path_10062" data-name="Path 10062"
                              d="M430.982,404.611c1.167-1.256,2.348-2.5,3.528-3.745.567-.6,1.149-1.188,1.737-1.773.562-.557,1.128-1.113,1.667-1.69a1.043,1.043,0,0,0-1.473-1.476,15.357,15.357,0,0,1-1.788,1.541c-.631.494-1.242,1.027-1.841,1.561-.618.551-1.229,1.1-1.814,1.682-.655.653-1.309,1.3-1.969,1.947a1.4,1.4,0,0,0,0,1.954,1.413,1.413,0,0,0,1,.4,1.3,1.3,0,0,0,.958-.4Z"
                              transform="translate(-425.808 -373.65)" fill="#0b2238" />
                            <path id="Path_10063" data-name="Path 10063"
                              d="M443.966,408.788a1.126,1.126,0,0,0,.766-.769,2.952,2.952,0,0,0,.007-1.3c-.033-.264-.072-.528-.111-.789-.042-.292-.084-.582-.125-.873-.083-.664-.168-1.328-.263-1.99-.039-.261-.088-.528-.148-.784-.07-.3-.125-.614-.215-.911-.394-1.279-.818-2.55-1.245-3.818a1.482,1.482,0,0,0-.655-.852,1.423,1.423,0,0,0-2.089,1.608c.546,1.537,1.105,3.075,1.564,4.64.2.694.38,1.4.559,2.095.2.767.385,1.539.528,2.319a1.1,1.1,0,0,0,.111.805,1.413,1.413,0,0,0,.469.5.99.99,0,0,0,.547.15,1.168,1.168,0,0,0,.3-.038Z"
                              transform="translate(-439.822 -378.053)" fill="#0b2238" />
                          </g>
                        </svg>
                        <span>Save up to 30%</span>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- End. tab -->

            <div class="tab-content content_pricing" id="pills-tabContent">
              <div class="tab-pane show active" id="pills-month" role="tabpanel" aria-labelledby="pills-month-tab">

                <div class="row no-gutters">
                   
                  <div class="col-lg-12 my-auto">
                    <div class="group_price_table checkbox-item">

                      <div class="fadein">
                        @foreach($currency as $key => $data)
                        <!-- item -->
                        <div class="item_price item-select">
                          <div class="part_one">
                            <span class="check_select"></span>
                            <h3>{{$data->name}} <span class="offer">{{$data->symbol}}</span></h3>
                          </div>
                          <div class="part_two">
                            <h4>{{number_format($data->buy, $basic->decimal)}} <span>/ USD</span></h4>
                          </div>
                        </div>
                        <!-- item -->
                        @endforeach
                         

                       
                      </div>

                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="pills-annually" role="tabpanel" aria-labelledby="pills-annually-tab">

                <div class="row no-gutters">
                   
                  <div class="col-lg-12 my-auto">
                    <div class="group_price_table checkbox-item">
                      <div class="fadein">

                        @foreach($currency as $key => $data)
                        <!-- item -->
                        <div class="item_price item-select">
                          <div class="part_one">
                            <span class="check_select"></span>
                            <h3>{{$data->name}} <span class="offer">{{$data->symbol}}</span></h3>
                          </div>
                          <div class="part_two">
                            <h4>{{number_format($data->sell, $basic->decimal)}} <span>/ USD</span></h4>
                          </div>
                        </div>
                        <!-- item -->
                        @endforeach

                       

                      </div>

                    </div>
                  </div>
                </div>


              </div>
            </div>

          </div>
        </section>
        <!-- End. p_pricing_list -->
 
 
        <!-- Start creative_box_contact -->
        <section class="creative_box_contact padding-t-12">
          <div class="container">
            <div class="content">
              <div class="row justify-content-center text-center">
                <div class="col-lg-5">
                  <div class="title_sections_inner margin-b-4">
                    <h2 class="c-white" data-aos="fade-up" data-aos-delay="0">Cryptocurrency Calculator</h2>
                    <p class="c-light" data-aos="fade-up" data-aos-delay="100">
                        <script>

function myFunction() {
 var amount = $('#mySelect2').val() ;

 var price = $("#mySelect option:selected").attr('data-price');
 var name = $("#mySelect option:selected").attr('data-name');
 var sell = $("#mySelect option:selected").attr('data-sell');
 var buy = $("#mySelect option:selected").attr('data-buy');
 var cur = $("#mySelect option:selected").attr('data-cur');
 var rate = Math.round(price).toFixed(2);

 var sellcharge = amount * sell /100;
 var buycharge = amount * buy /100;
 var paybuy = 1*amount-buycharge;
 var paysell = 1*amount+sellcharge;
 var rate = parseFloat(1*amount/price).toFixed(8);

 document.getElementById("unit").innerHTML = "What you get: " + rate + cur;

 document.getElementById("buy").innerHTML = "We buy at: USD " + paybuy;
 document.getElementById("sell").innerHTML = "We sell at: USD " + paysell;
 var unit = parseFloat(amount / price).toFixed(8);
 document.getElementById("price").innerHTML = "USD " +      Math.round(rate).toFixed(2);

 };
</script>


<section class=" "><div class="container "><br>

<div class="field-item"><label class="field-label text-white">Select Cryptocurrency</label><div class="field-wrap">
<select onchange="myFunction()" class="selec form-control"  id="mySelect"><option value="">Please select</option>
@foreach($currency as $key => $data)
<option data-cur="{{$data->symbol}}" data-sell="{{$data->sell}}"  data-name="{{$data->name}}"  data-price="{{$data->price}}" data-buy="{{$data->buy}}">{{$data->name}} </option>
@endforeach
</select></div> </div>
<br>

<div class="field-item"><label class="field-label text-white">Enter amount in <code> USD </code> </label> <div class="field-wrap"><input  id="mySelect2" onkeyup="myFunction()"  type="number" class="form-control" required></div></div>
<br>

<p id="buy"></p>
<p id="sell"></p>
<p id="unit"></p>


</div></section>
                        
                        
                    </p>
                  </div>
                   
              </div>
            </div>

          </div>
        </section>
        <!-- End. creative_box_contact -->


      </main>
    </div>
    <!-- [id] content -->

@endsection
